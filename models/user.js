/**
 *
 * @author Jitender
 */

// External Modules
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

// user schema
var userSchema = new Schema({
  Name                : { type: String, required: true },
  Email               : { type: String, required: true, unique: true },
  Phone               : { type: Number, required: true, unique: true },
  Password            : { type: String, required: true }
}, { timestamps: true }, { collection: 'users' });

// model
var User = mongoose.model('User', userSchema);

// make this available to our users in our applications
module.exports.User = User;