var express = require('express');
var router = express.Router();
var User = require('../models/user').User;

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'GPA-Home' });
});

router.get('/login', function(req, res, next) {
  res.render('loginform', { title: 'GPA-Login' });
});

router.get('/register', function(req, res, next) {
  res.render('registerform', { title: 'GPA-Register' });
});

router.post('/userLogin', function(req, res, next) {
  	console.log(req.body);
  	var data = req.body;
  	if(!data.email || !data.password){
  		res.render('registerform',{
  			error : 'Missing Parameters... try again'
  		});
  	} else {
		User.find({Email : data.email}, function(err, response){
			if(err || !response[0]){
	    		res.render('loginform', {
	    			title : 'GPA - User not found',
	    			error : 'User not found'
	    		});
			} else {
				req.session.isValid = true;
	            req.session.user = {};
	            req.session.user.name = response[0].Name;
	            req.session.user.email = response[0].Email;
	            req.session.user.phone = response[0].Phone;				    		
	    		res.render('index', {
	    			title : 'GPA - User loged in',
	    			error : 'User loged in',
	    			data : response[0],
	    			session : req.session
	    		});				
			}
		});	
  		
  	}  		
});

router.post('/userRegister', function(req, res, next) {
 	console.log(req.body);
  	var data = req.body;
  	if(!data.name || !data.phone || !data.email || !data.password || !data.repassword){
  		res.render('registerform',{
  			error : 'Missing Parameters... try again'
  		});
  	} else {
  		if(data.password != data.repassword){
	  		res.render('registerform',{
	  			error : 'Password do not match... try again'
	  		});
  		} else {
  			User.find({Email : data.email}, function(err, response){
  				if(response[0]){
			  		res.render('registerform',{
			  			error : 'user already exits... try different email/phone'
			  		});
  				} else {
				  	var newUser = new User({
				    	Name: data.name,
				    	Email: data.email,
				    	Phone: data.phone,
				    	Password: data.password
				  	});
				  	
				  	newUser.save(function(err, user) {
				    	console.log(err);
				    	if (err){
					  		res.render('registerform',{
					  			error : 'user already exits... try different email/phone'
					  		});
				    	} else {
							req.session.isValid = true;
				            req.session.user = {};
				            req.session.user.name = user.Name;
				            req.session.user.email = user.Email;
				            req.session.user.phone = user.Phone;				    		
				    		res.render('index', {
				    			title : 'GPA - User loged in',
				    			error : 'User loged in',
				    			data : user,
				    			session : req.session
				    		});
				    	}
					});
  				}
  			});	
  		}
  	} 	
  /*res.render('registerform', { title: 'GPA-Register' });*/
});

router.post('/resetPassword', function(req, res, next) {
  	console.log(req.body);
  	var data = req.body;
  	if(!data.email){
  		res.render('registerform',{
  			error : 'Missing Parameters... try again'
  		});
  	} else {
		User.find({Email : data.email}, function(err, response){
			if(err || !response[0]){
	    		res.render('loginform', {
	    			title : 'GPA - User not found',
	    			error : 'User not found'
	    		});
			} else {
				var newPass = generatePassword();
				User.Update({ Email : data.email}, function(err){
					if(err){
						res.render('index', {
			    			title : 'GPA - User loged in',
			    			error : 'Please try again'
			    		});		
					} else {
						res.render('index', {
			    			title : 'GPA - User loged in',
			    			error : 'New Password',
			    			password : newPass
			    		});		
					}
				});				
			}
		});	
  		
  	}  		
});

function generatePassword() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 8; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

module.exports = router;
